"""
Module to simplify import and fitting of SMaRT impedance data based on impedance.py.
"""

__author__ = "James Cumby"
__email__ = "james.cumby@ed.ac.uk"


from impedance import preprocessing
from impedance.models import circuits
import cmath
import pandas as pd
import numpy as np


def read_SMaRT(csv_data):
    """ Read SMaRT CSV data into Pandas DataFrame, generating Real/Imaginary components. """
    
    # SMaRT CSV files have trailing commas on data rows; use index_col=False to avoid problems
    data  = pd.read_csv(csv_data, header=2, index_col=False)
    
    data['Complex Impedance (Ohms)'] = data[['Impedance Magnitude (Ohms)','Impedance Phase Degrees (\')']].apply(lambda x: cmath.rect(x[0], np.radians(x[1])), axis=1)
    
    return data
    
    
def filter_frequencies(data, f_min=None, f_max=None, f_ind=[]):
    """ Remove unwanted frequencies from impedance spectrum. 
    
    Parameters
    ----------
    
    data : pandas DataFrame
        DataFrame of impedance containing 'Frequency (Hz)' column
    f_min : float
        Minimum frequency to keep (inclusive)
    f_max : float
        Maximum frequency to keep (inclusive)
    f_ind : iterable of floats
        Individual frequencies to exclude from data
        
    Returns
    -------
    
    data : pandas DataFrame
        copy of original DataFrame with filtered frequencies removed.
    """
    
    expr = data['Frequency (Hz)'] == data['Frequency (Hz)']
    if f_min is not None:
        expr = expr & (data['Frequency (Hz)'] >= f_min)
    if f_max is not None:
        expr = expr & (data['Frequency (Hz)'] <= f_max)
    for f in f_ind:
        expr = expr & (~np.isclose(data['Frequency (Hz)'], f))
        
    return data[expr]
    
    
def filter_impedance(data, Z_min=None, Z_max=None):
    """ Remove unwanted frequencies from impedance spectrum. 
    
    Parameters
    ----------
    
    data : pandas DataFrame
        DataFrame of impedance containing 'Impedance Magnitude (Ohms)' column
    Z_min : float
        Minimum impedance magnitude to keep (inclusive)
    Z_max : float
        Maximum impedance magnitude to keep (inclusive)
        
    Returns
    -------
    
    data : pandas DataFrame
        copy of original DataFrame with filtered frequencies removed.
    """
    
    expr = data['Impedance Magnitude (Ohms)'] == data['Impedance Magnitude (Ohms)']
    if Z_min is not None:
        expr = expr & (data['Impedance Magnitude (Ohms)'] >= Z_min)
    if Z_max is not None:
        expr = expr & (data['Impedance Magnitude (Ohms)'] <= Z_max)
        
    return data[expr]
    
    
    
def subtract_background(data,
                        background,
                       ):
    """ Remove background contribution from leads etc. 
    
    Parameters
    ----------
    
    data : DataFrame
        DataFrame of measured impedance data containing (as minimum) the columns
        'Frequency (Hz)', 'AC Level (V)', 'DC Level (V)', 'Complex Impedance (Ohms)'
    background : DataFrame
        DataFrame of background impedance data (i.e. short circuit) to subtract from
        `data`. Frequency, AC and DC levels must match the measured data (i.e. same 
        impedance measurement sequence).
        
        
    Returns
    -------
    
    newdata : DataFrame
        Copy of `data` with Impedance values corrected by subtracting background.
        
        
    Notes
    -----
    
    - Capacitance (F) is incorrectly calculated somewhere, so is (currently) being removed.
    """
    
    # Merge based on Frequency, AC and DC level

    merged = data.merge(background, 
                        on=['Frequency (Hz)', 'DC Level (V)', 'AC Level (V)'],
                        how='left',
                        suffixes=('','_bkg')).dropna(axis=0)

    if data.shape[0] != merged.shape[0]:
        print ("Warning: Background frequencies do not match with data frequencies")
        
    newdata = data.copy()
    
    newdata['Complex Impedance (Ohms)'] = merged['Complex Impedance (Ohms)'] - merged['Complex Impedance (Ohms)_bkg']
    
    newdata['Impedance Phase Degrees (\')'] = newdata['Complex Impedance (Ohms)'].apply(cmath.phase)
    newdata['Impedance Magnitude (Ohms)'] = newdata['Complex Impedance (Ohms)'].abs()
    newdata['Admittance Magnitude (S)'] = 1.0/ newdata['Impedance Magnitude (Ohms)']
    
    
    
    return newdata.drop('Capacitance Magnitude (F)', axis=1)
    
    
def fit_spectrum(data,
                 circuit,
                 initial_guesses,
                 constants = {},
                 allow_negative_bounds=False,
                 **kwargs,
                ):
    """Fit impedance model to f,Z data and return parameters. 
    
    
    Parameters
    ----------
    
    data : DataFrame
        DataFrame containing 'Frequency (Hz)' and 'Complex Impedance (Ohms)' columns
    circuit : string
        Model definition to pass to impedance.py
    initial_guesses : list or dict
        Initial parameter guesses for model
    constants : dict
        Parameters to be treated as constants during fitting
        
        
    Returns
    -------
    
    
    
    
    Notes
    -----
    
    Circuit definition
    
        Models are defined using strings as in impedance.py. Series elements should be separated by '-' (e.g. 'R1-C1')
        while parallel elements are denoted by 'p(...)', e.g. 'p(R1,C1)'. Elements can be numbered either directly ('R0') or
        with an underscore ('R_0'). See https://impedancepy.readthedocs.io/en/latest/index.html for more details.
        
    Initial Guesses
        
        Initial parameters for fitting can be provided either as a list (like impedance.py) or as a dict of {component:value}
        pairs. For circuit components with multiple parameters (e.g. CPE) these are entered as a list/tuple in order, e.g
        {component: (value1, value2)}.
    
    """
    
    assert isinstance(circuit, str)
    assert isinstance(initial_guesses, dict) or isinstance(initial_guesses, list)
    
    if isinstance(initial_guesses, dict):
        # Convert dict of guesses to list in correct order
        comp_idx = {}
        for comp in initial_guesses:
            try:
                pos = circuit.index(comp)
            except ValueError:
                continue
            
            comp_idx[pos] = comp
            
        guesses = []
        for idx in sorted(comp_idx.keys()):
            if isinstance(initial_guesses[comp_idx[idx]], tuple) or isinstance(initial_guesses[comp_idx[idx]], list):
                values = [i for i in initial_guesses[comp_idx[idx]]]
            else:
                values = [initial_guesses[comp_idx[idx]]]
            
            for v in values:
                guesses.append(v)

    else:
        guesses = initial_guesses
        
    
    model = circuits.CustomCircuit(initial_guess=guesses,
                                  circuit = circuit,
                                  constants=constants
                                 )    
        
    f = data['Frequency (Hz)'].to_numpy()
    Z = data['Complex Impedance (Ohms)'].to_numpy()
        
    bounds = circuits.fitting.set_default_bounds(circuit, constants=constants)
    if allow_negative_bounds:
        bounds = ([-i for i in bounds[1]], bounds[1])
        
    model.fit(f,Z, bounds=bounds, **kwargs)
    
    
    # Assign values to model
    model.frequency_data = f
    model.Z_data = Z
    if "Temperature ('C)" in data.columns:
        model.temp = data["Temperature ('C)"].mean()
        model.temp_std = data["Temperature ('C)"].std()
    if "AC Level (V)" in data.columns:
        model.ac = data["AC Level (V)"].mean()
        model.ac_std = data["AC Level (V)"].std()
    if "DC Level (V)" in data.columns:
        model.dc = data["DC Level (V)"].mean()
        model.dc_std = data["DC Level (V)"].std()
        
    return model
    
    
