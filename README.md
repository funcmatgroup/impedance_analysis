# Impedance Analysis

This repository is intended to simplify impedance analysis of ceramic samples 
measured under variable temperature conditions, in particular using the SMaRT
software.

It relies heavily on [impedance.py](https://impedancepy.readthedocs.io/en/latest/index.html)
but with some added functionality for reading SMaRT datasets and processing multi-temperature
data.

# Usage

Assuming you wish to process some data measured using SMaRT (and exported as a CSV file)
the steps are as follows:

1. Import the `smart_fit.py` module into Python (here we are using a Jupyter notebook)
	- If `smart_fitting` is in a local directory, add that location to PATH before importing:
		``` python
		import sys
		sys.path.append("/path/to/smart_fitting/")
		import smart_fit
		```
	
2. Read in a CSV file generated with SMaRT:
	``` python
	data = smart_fit.read_SMaRT("/path/to/data/file.csv")
	```
   Note that `data` is a Pandas DataFrame containing data for all temperatures
3. (Optionally) subtract a background signal (note that it must be measured with the same frequency range,
    AC and DC levels):
	
	``` python
	background = smart_fit.read_SMaRT("/path/to/closed/circuit/collection")
	corrected = smart_fit.subtract_background(data, background)
	```
4. (Optionally) filter outliers based on frequency values or impedance magnitude
	``` python
	filtered = smart_fit.filter_frequencies(corrected, f_max = 10E6)
	filtered = smart_fit.filter_impedance(filtered, Z_max = 100E6)
	```
5. *Fit a model to the data*. This uses [impedance.py](https://impedancepy.readthedocs.io/en/latest/index.html)
   syntax to define equivalent circuit models. In short, individual elements are defined by a string expression
   with an optional numerical label 
   (e.g. `R`, `R0` or `R_0` for a resistor). Components connected in series are linked by `-` while parallel
   groups are linked by `p(...)`. These can be combined as needed to give complex circuits.
   
   For our purposes, the most common elements (and the relationship between impedance $`Z`$ and parameters $`P_n`$ are:
	
   - resistor `R` $`(Z = R)`$
   - Capacitor `C` $`(Z = \frac{1}{j  2\pi f \times C})`$
   - Inductance `L` $`(Z = L \times j 2\pi f)`$
   - Constant Phase element (capacitor with non-90 degree phase shift) `CPE` $`(Z = \frac{1}{P_0 \times (j 2 \pi f)^{P_1}})`$
   	- Note, the CPE has two parameters
   	
   Other elements (e.g. Warburg elements) are listed [here](https://impedancepy.readthedocs.io/en/latest/circuit-elements.html)
	
   For solid state samples exhibiting two semicircles, a common equivalent circuit is:
   ```
   "p(R0, CPE0)-p(R1, CPE1)"
   ```
   
   a. *Extract the data required*. To fit this model to our data, we first need to extract a dataset from one temperature. To extract just the 500C data, for instance:
   
	  ``` python
	  Tdata = filtered[(filtered["Temperature ('C)"] >= 498) & (filtered["Temperature ('C)"] <= 502) ]
	  ```
   	
   	  alternatively, we can use Pandas `groupby` operation to create groups of data based on Set Point. Optionally you may also
   	  want to group by AC level if you have measured multiple conditions:
   	
   	  ``` python
   	  groups = filtered.groupby(['Set Point (\'C)', 'AC Level (V)'])
   	  Tdata = groups.get_group((500, 0.1))
   	  ```
   	
   b. *Construct and fit the model.* To fit a model to these data, we need to define the equivalent circuit, initial parameter guesses and (optionally)
   	  any constant parameters (e.g. a constant offset resistance determined from a different dataset).
   	  These are passed to `fit_spectrum` along with the data:
   	
   	  ``` python
   	  fit = smart_fit.fit_spectrum(Tdata,
   		model="p(R0, CPE0)-p(R1, CPE1)", 
   		initial_guesses = {'R0': 1500, 'CPE0': (1E-12, 1), 'CPE1': (1E-10, 1)},
   		constants = {'R1': 1.53E5},
   		)
   	  ```
	  
	  Note that `fit_spectrum` can pass additional arguments to the `fit` function of an `impedance.py` circuit, which in turn passes these arguments to
	  `scipy.optimise.curve_fit`. In particular, passing `xtol=None` seems to have a big impact on the quality of solution obtained, at the expense of
	  taking more time to fit (good if your initial guesses are some way from the ideal).
   	
   c. *Visualise the results.* The resulting `fit` object is generated from `impedance.py` and contains the fitted parameters, original data and functions 
   	  for plotting and analysis.
   	  To see the refined parameters:
   	
   	  ``` python
   	  print(fit)
   	  ```
   	
   	  If you want to plot the results of the model and the data, the easiest way is to use the model's `plot` function, passing
   	  in the data to plot measured points:
   	  ``` python
   	  fit.plot(f_data = fit.frequency_data, Z_data = fit.Z_data)
   	  ```
6. Repeat for multiple temperatures. To fit the same model over different temperatures and store the results, a simple approach based
   on looping through the temperatures is as follows:
	
   ``` python
   groups = filtered.groupby(['Set Point (\'C)', 'AC Level (V)'])
   models = {}
   for ID, Tdata in groups:
        models[ID] = smart_fit.fit_spectrum(Tdata,
        model="p(R0, CPE0)-p(R1, CPE1)", 
        initial_guesses = {'R0': 1500, 'CPE0': (1E-12, 1), 'CPE1': (1E-10, 1)},
        constants = {'R1': 1.53E5},
        )
   ```
   Here, individual fits can be extracted using the (T, AC) syntax from the groups:
   ``` python
   print(models[(500, 0.1)])
   ```
	
        
   
	
# Authors

James Cumby 2022
